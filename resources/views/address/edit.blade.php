@extends('layouts.admin-panel.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Validation</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Address
                </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{route('addresses.update',$address->id)}}" method="POST" id="quickForm">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea name="address" id="address" cols="30" rows="10" class="form-control @error('address') is-invalid @enderror" placeholder="Enter Address" required>{{old('address',$address->address)}}</textarea>

                        @error('address')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">

                        <label for="landmark">Landmark</label>
                        <input type="text" name="landmark" class="form-control @error('landmark') is-invalid @enderror" id="landmark" placeholder="Enter Landmark" value="{{old('landmark',$address->landmark)}}" required>

                        @error('landmark')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input type="text" name="city" class="form-control @error('city') is-invalid @enderror" id="city" placeholder="Enter City"  value="{{old('city',$address->city)}}" required>
                        @error('city')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="district">District</label>
                        <input type="text" name="district" class="form-control @error('district') is-invalid @enderror" id="district" placeholder="Enter District"  value="{{old('district',$address->district)}}" required>
                        @error('district')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="state">State</label>
                        <input type="text" name="state" class="form-control @error('state') is-invalid @enderror" id="state" placeholder="Enter State"  value="{{old('state',$address->state)}}" required>
                        @error('state')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="postal_code">Postal Code</label>
                        <input type="text" name="postal_code" class="form-control @error('postal_code') is-invalid @enderror" id="postal_code" placeholder="Enter Postal Code"  value="{{old('postal_code',$address->postal_code)}}" required>
                        @error('postal_code')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Next</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
