@extends('layouts.admin-panel.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Address</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Address</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Address</th>
                    <th>Landmark</th>
                    <th>City</th>
                    <th>District</th>
                    <th>State</th>
                    <th>Postal Code</th>
                    <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($addresses as $address)
                        <tr>

                            <td>{{$address->address}}</td>
                            <td>{{$address->landmark}}</td>
                            <td>{{$address->city}}</td>
                            <td>{{$address->district}}</td>
                            <td>{{$address->state}}</td>
                            <td>{{$address->postal_code}}</td>
                            <td><a href="{{route('addresses.edit',$address->id)}}" class="btn btn-warning">Edit</a>
                                <form action="{{route('addresses.destroy',$address->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="mt-2 btn btn-danger">Delete</button>
                                </form></td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection

