@extends('layouts.admin-panel.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Trash</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Trash</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Acknowledge</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Collecting From</th>
                    <th>Type</th>
                    <th>Collecting Person</th>
                    <th>Weight</th>
                    <th>booked on</th>
                    <th>id</th>
                    <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($trashes as $trash)
                        <tr>
                            <td>{{$trash->collecting_address()}}</td>
                            <td>{{$trash->type}}</td>
                            <td>{{$trash->getCollectingPersonInfo()}}</td>
                            <td>{{$trash->getWeightInfo()}}</td>
                            <td>{{$trash->updated_at}}</td>
                            <td>{{$trash->id}}</td>
                            <td>
                                <form action="{{route('trashes.acceptAcknowledge',$trash->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-success">
                                        Accept
                                    </button>
                                </form>

                                <form action="{{route('trashes.rejectAcknowledge',$trash->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <button class="btn btn-danger">
                                        Reject
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('page-level-scripts')
<script>
function insertWeightForm(trashId)
    {
        var url = "/trashes/picked/"+trashId;
        $("#weightForm").attr('action',url);
    }
</script>
@endsection
