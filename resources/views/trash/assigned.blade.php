@extends('layouts.admin-panel.app')
@section('content')
<div class="content-wrapper">
    {{-- modal for assigning pickup --}}
    <div class="modal fade" id="insertWeight">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Insert Weight</h4>
            </div>
            <div class="modal-body">
                <form action="" id="weightForm" method="POST">
                    @csrf
                    @method('PUT')
                    <label for="weight">
                        Weight:
                    </label>
                    <input type="number" name="weight">

                  </div>
                  <div class="modal-footer justify-content-between">

                    {{-- <a href="{{route('trashes.assign')}}" class="btn btn-primary">Assign</a> --}}
                    <button type="submit" class="btn btn-primary">
                        Submit
                    </button>
                  </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Trash</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Trash</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Collecting From</th>
                    <th>Type</th>
                    <th>Weight</th>
                    <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach ($trashes as $trash)
                        <tr>

                            <td>{{$trash->collecting_address()}}</td>
                            <td>{{$trash->type}}</td>
                            <td>{{$trash->getWeightInfo()}}</td>
                            <td><button class="btn btn-success" onclick="insertWeightForm({{$trash->id}})" data-toggle="modal" data-target="#insertWeight">
                                    Picked
                                </button>
                                <form action="{{route('trashes.reject',$trash->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                  <button type="submit" class="btn btn-danger" >
                                        Reject
                                  </button>
                                </form>

                            </td>

                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
@section('page-level-scripts')
<script>
function insertWeightForm(trashId)
    {
        var url = "/trashes/picked/"+trashId;
        $("#weightForm").attr('action',url);
    }
</script>
@endsection
