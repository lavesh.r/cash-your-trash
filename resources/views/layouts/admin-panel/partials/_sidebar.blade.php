 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link text-center">
      <span class="brand-text font-weight-light text-center">Cash Your Trash</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{auth()->user()->gravatarImage}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{auth()->user()->name}}</a>
        </div>
      </div>



      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        @if (auth()->user()->isAdmin())
        <li class="nav-item">
            <a href="{{route('employees.create-basic')}}" class="nav-link">
                <i class="nav-icon fa fa-plus"></i>
                <p>
                    Add New Employee
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('employees.index')}}" class="nav-link">
                <i class="nav-icon fa fa-eye"></i>
                <p>
                    View All Employees
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('trashes.pending')}}" class="nav-link">
              <i class="nav-icon fa fa-plus"></i>
              <p>
                Trash Pending
              </p>
            </a>
          </li>
        @endif
        @if (auth()->user()->isEmployee())
            <li class="nav-item">
                <a href="{{route('trashes.assigned')}}" class="nav-link">
                <i class="nav-icon fa fa-plus"></i>
                <p>
                    Assigned Bookings
                </p>
                </a>
            </li>
        @endif

          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fa fa-plus"></i>
              <p>
                New Booking
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('trashes.index')}}" class="nav-link">
              <i class="nav-icon fa fa-clock-o"></i>
              <p>
                Booking History
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Trash
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('trashes.create')}}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Create-Trash
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('trashes.acknowledge')}}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Pending Acknowledgements
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('addresses.index')}}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Address
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('addresses.create')}}" class="nav-link">
              <i class="nav-icon fa fa-th"></i>
              <p>
                Create-Address
              </p>
            </a>
          </li>
          <li class="nav-item">
            <form action="{{route('logout')}}" method="POST">
                @csrf
                <button id="logout-btn" type="submit"><i class="fa fa-sign-out "></i> Logout</button>
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
