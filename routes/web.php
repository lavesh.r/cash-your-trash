<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\TrashController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth','admin'])->group(function(){
    Route::get('employees/create-basic',[EmployeeController::class,'create_basic'])->name('employees.create-basic');
    Route::get('employees/create-address',[EmployeeController::class,'create_address'])->name('employees.create-address');
    Route::get('employees/create-post',[EmployeeController::class,'create_post'])->name('employees.create-post');

    Route::post('employees/store-basic',[EmployeeController::class,'store_basic'])->name('employees.store-basic');
    Route::post('employees/store-address',[EmployeeController::class,'store_address'])->name('employees.store-address');
    Route::resource('employees', EmployeeController::class)->except('create');
    Route::get('trashes/pending', [TrashController::class,'adminPending'])->name('trashes.pending');
    Route::put('trashes/assign/{trash}', [TrashController::class,'adminAssign'])->name('trashes.assign');
});
Route::middleware(['auth','employee'])->group(function(){
    Route::get('trashes/assigned',[TrashController::class,'assigned'])->name('trashes.assigned');
    Route::put('trashes/picked/{trash}',[TrashController::class,'picked'])->name('trashes.picked');
    Route::put('trashes/reject/{trash}',[TrashController::class,'reject'])->name('trashes.reject');
});
Route::middleware(['auth'])->group(function(){

    Route::put('trashes/acceptAcknowledge/{trash}',[TrashController::class,'acceptAcknowledge'])->name('trashes.acceptAcknowledge');
    Route::put('trashes/rejectAcknowledge/{trash}',[TrashController::class,'rejectAcknowledge'])->name('trashes.rejectAcknowledge');
    Route::get('trashes/acknowledge',[TrashController::class,'acknowledge'])->name('trashes.acknowledge');


    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('trashes', TrashController::class);
    Route::resource('addresses', AddressController::class);


});
