<?php

namespace App\Http\Requests\employee;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'address' => 'required',
            'landmark' => 'required',
            'city' => 'required',
            'district' => 'required',
            'state' => 'required',
            'postal_code' => 'required|digits:6'

        ];
    }
}
