<?php

namespace App\Http\Requests\employee;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeBasicsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->role === User::ADMIN;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email'=> 'unique:users',
            'phone' => 'required|digits:10|unique:contacts'
        ];
    }
}
