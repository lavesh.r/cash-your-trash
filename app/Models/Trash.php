<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Trash extends Model
{
    use HasFactory;
    //constants
    const BOOKED = 'booked';
    const ASSIGNED = 'assigned';
    const PICKED = 'picked';
    const REJECT = 'reject';
    const USERACKNOWLEDGEMENT = 'waiting for user acknowledgement';
    const REJECTBYUSER = 'reject by user';

    protected $guarded = ['id'];
    public function collecting_point()
    {
        return $this->belongsTo(Address::class,'address_id');
    }
    public function collecting_address()
    {
        return $this->collecting_point->address." ".$this->collecting_point->city." ".$this->collecting_point->postal_code;
    }
    public function getCollectingPersonInfo()
    {
        if($this->assigned_to == null)
            return "-";
        return $this->getAssignedEmployee()->loginDetails->name;
    }
    public function getWeightInfo()
    {
        if($this->weight == 0)
            return "-";
        return $this->weight;
    }
    public function getAssignedEmployee()
    {
        return Employee::find($this->assigned_to);
    }
    public function owner()
    {
        return User::findOrFail($this->collecting_point->user_id);
    }

    //scopes
    public function scopeAddress($query)
    {
        return $query->join('addresses',function($join)
        {
            $join->on('trashes.address_id','=','addresses.id');
        });
    }
    public function scopeUserTrashes($query)
    {
        return $query->where('user_id','=',auth()->id());
    }
    public function scopePending($query)
    {
        return $query->where('status','=',Trash::BOOKED);
    }
    public function scopeAcknowledge($query)
    {
        return $query->where('status','=',Trash::USERACKNOWLEDGEMENT);
    }
    public function scopeAssigned($query)
    {
        return $query->where('status','=',Trash::ASSIGNED)->where('assigned_to','=',User::getEmployeeId(auth()->id()));
    }
}
