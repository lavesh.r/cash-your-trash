<?php

namespace App\Providers;

use App\Models\Address;
use App\Models\Employee;
use App\Models\Trash;
use App\Policies\AddressPolicy;
use App\Policies\EmployeePolicy;
use App\Policies\trash\TrashUpdatePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Address::class => AddressPolicy::class,
        Trash::class => TrashUpdatePolicy::class


    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
